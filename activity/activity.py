# 1. Create 5 variables and output them in the command prompt in the following format:
# "I am < name (string)>, and I am <age (integer) years old, I work as a <occupation (string)>, and my rating for <movie (string)> is <rating (decimal)> %"

name = 'Edmar'
age = 24
occupation = 'IT Support'
movie = 'Who am I: No System Is Safe'
rating = 98.9

print(f'I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating} %')

# 2. Create 3 variables, num1, num2 and num3
num1, num2, num3 = 10, 15, 25

# get the product of num1 and num2
print(f'The product of num1 and num2 is {num1 * num2}')

# check if num1 is less than num3
print(f'Is num1 less than num3 -> {num1 < num3}')

# add the value of num3 to num2
print(f'The sum of num3 and num2 is {num3 + num2}')